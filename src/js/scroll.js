(function () {
    var SCROLL_RANGE = 50;
    var header = document.querySelector('.header');

    window.addEventListener('scroll', function () {
        if(window.pageYOffset >= SCROLL_RANGE) {
            header.classList.add('header--scroll');
        } else {
            header.classList.remove('header--scroll');
        }
    });

})();
