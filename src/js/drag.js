'use strict';

(function () {
    var BOX_WIDTH = 300;
    var BOX_HEIGHT = 300;
    var LEFT_MOUSE_CLICK = 0;
    var box = document.querySelector('#box');
    var image = box.querySelector('#picture');

    image.addEventListener('mousedown', function (evt) {
        evt.preventDefault();

        if (evt.button === LEFT_MOUSE_CLICK) {
            var startCoords = {
                x: evt.clientX,
                y: evt.clientY
            };

            var imageWidth = image.clientWidth;
            var imageHeight= image.clientHeight;
            var coordsMap = {
                maxX: 0,
                minX: BOX_WIDTH - imageWidth,
                maxY: 0,
                minY: BOX_HEIGHT - imageHeight
            };

            var pictureMoveMouseHandler = function (moveEvt) {
                moveEvt.preventDefault();
                var shift = {
                    x: startCoords.x - moveEvt.clientX,
                    y: startCoords.y - moveEvt.clientY
                };

                var currentCoords = {
                    x: image.offsetLeft - shift.x,
                    y: image.offsetTop - shift.y
                };

                startCoords = {
                    x: moveEvt.clientX,
                    y: moveEvt.clientY
                };

                image.style.left = currentCoords.x + 'px';
                image.style.top = currentCoords.y + 'px';

                if (coordsMap.maxX < currentCoords.x) {
                    image.style.left = coordsMap.maxX + 'px'
                }

                if (coordsMap.minX > currentCoords.x) {
                    image.style.left = coordsMap.minX + 'px';
                }

                if (coordsMap.maxY < currentCoords.y) {
                    image.style.top = coordsMap.maxY + 'px';
                }

                if (coordsMap.minY > currentCoords.y) {
                    image.style.top = coordsMap.minY + 'px';
                }
            };

            var pictureUpMouseHandler = function (upEvt) {
                upEvt.preventDefault();

                document.removeEventListener('mousemove', pictureMoveMouseHandler);
                document.removeEventListener('mouseup', pictureUpMouseHandler);
            };

            document.addEventListener('mousemove', pictureMoveMouseHandler);
            document.addEventListener('mouseup', pictureUpMouseHandler);
        }
    });
})();
