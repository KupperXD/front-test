'use strict';

(function ($) {
    var BOX_WIDTH = 300;
    var BOX_HEIGHT = 300;
    var INDENT = 50;
    var indentIntersection = INDENT / 2;
    var box = document.querySelector('#box-2');
    var picture = box.querySelector('#picture-2');
    var $pictureJ = $('#picture-2');

    var getCoords = function (elem) {
        var block = elem.getBoundingClientRect();

        return {
            top: block.top + window.pageYOffset,
            left: block.left + window.pageXOffset
        };
    };

    var getFlag = function (coordsMin, coordsMax, direction) {
        var flag = (direction >= coordsMin + indentIntersection && direction <= coordsMax - indentIntersection) ? true : false;

        return flag;
    };

    var checksCoords = function (min, max, evt) {
        var flag = (evt > min + INDENT || evt < max - INDENT) ? true : false;

        return flag;
    }

    box.addEventListener('mouseenter', function (evt) {
        evt.preventDefault();

        var pictureWidth = picture.clientWidth;
        var pictureHeight = picture.clientHeight;
        var coordinateBox = getCoords(box);
        var coordsBoxMap = {
            x: {
                min: coordinateBox.left,
                max: coordinateBox.left + BOX_WIDTH
            },
            y: {
                min: coordinateBox.top,
                max: coordinateBox.top + BOX_HEIGHT
            }
        };
        var coordsPictureMap = {
            x: {
                min: BOX_WIDTH - pictureWidth,
                max: 0
            },
            y: {
                min: BOX_HEIGHT - pictureHeight,
                max: 0
            }
        };

        var movePicture = function (pageX, pageY) {
            var flagTopBottom = getFlag(coordsBoxMap.x.min, coordsBoxMap.x.max, pageX);
            var flagLeftRight = getFlag(coordsBoxMap.y.min, coordsBoxMap.y.max, pageY);

            if (flagTopBottom && pageY <= coordsBoxMap.y.min + INDENT) {
                $pictureJ.stop().animate({top: coordsPictureMap.y.max + 'px'}, 4000);
            }

            if (flagTopBottom && pageY + INDENT >= coordsBoxMap.y.max) {
                $pictureJ.stop().animate({top: coordsPictureMap.y.min + 'px'}, 4000);
            }

            if (flagLeftRight && pageX <= coordsBoxMap.x.min + INDENT) {
                $pictureJ.stop().animate({left: coordsPictureMap.x.max + 'px'}, 4000);
            }

            if (flagLeftRight && pageX + INDENT >= coordsBoxMap.x.max) {
                $pictureJ.stop().animate({left: coordsPictureMap.x.min + 'px'}, 4000);
            }

        };

        var boxMouseMoveHandler = function (moveEvt) {
            moveEvt.preventDefault();
            var flagX = checksCoords(coordsBoxMap.x.min, coordsBoxMap.x.max, moveEvt.pageX);
            var flagY = checksCoords(coordsBoxMap.y.min, coordsBoxMap.y.max, moveEvt.pageY);

            if (flagX && flagY) {
                $pictureJ.stop();
            }

            movePicture(moveEvt.pageX, moveEvt.pageY);
        };

        var boxMouseLeaveHandler = function (leaveEvt) {
            leaveEvt.preventDefault();
            $pictureJ.stop(true);

            box.removeEventListener('mousemove', boxMouseMoveHandler);
            box.removeEventListener('mouseleave', boxMouseLeaveHandler);
        }

        movePicture(evt.pageX, evt.pageY);
        box.addEventListener('mousemove', boxMouseMoveHandler);
        box.addEventListener('mouseleave', boxMouseLeaveHandler);

    });
})(jQuery);
