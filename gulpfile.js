'use strict';

var gulp = require("gulp");
var plumber = require("gulp-plumber");
var sourcemap = require("gulp-sourcemaps");
var rename = require("gulp-rename");
var sass = require("gulp-sass");
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var csso = require("gulp-csso");
var server = require("browser-sync").create();
var del = require("del");
var jsmin = require("gulp-jsmin");
var htmlmin = require("gulp-htmlmin");
var concat = require("gulp-concat");

gulp.task("css", function () {
    return gulp.src("src/scss/styles.scss")
        .pipe(sourcemap.init())
        .pipe(sass())
        .pipe(postcss([
            autoprefixer()
            ]))
        .pipe(csso())
        .pipe(rename("styles.min.css"))
        .pipe(sourcemap.write("."))
        .pipe(gulp.dest("dist/css"))
        .pipe(server.stream());
});

gulp.task("html", function () {
    return gulp.src("src/*.html")
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest("dist"));
});

gulp.task("js", function () {
    return gulp.src("src/js/*.js")
        .pipe(concat("all.js"))
        .pipe(jsmin())
        .pipe(rename("scripts.min.js"))
        .pipe(sourcemap.write("."))
        .pipe(gulp.dest("dist/js"));
});

gulp.task("jsLib", function () {
    return gulp.src("src/js/lib/*.js")
        .pipe(concat("all.js"))
        .pipe(jsmin())
        .pipe(rename("vendor.min.js"))
        .pipe(gulp.dest("dist/js"));
});

gulp.task("clean", function () {
    return del("dist");
});

gulp.task("server", function () {
    server.init({
        server: "dist/",
        notify: false,
        oper: true,
        cors: true,
        ui: false
    });

    gulp.watch("src/scss/**/*.{scss, sass}", gulp.series("css"));
    gulp.watch("src/js/*.js", gulp.series("js"));
    gulp.watch("src/*.html", gulp.series("html")).on("change", server.reload);
});

gulp.task("dist", gulp.series(
    "clean",
    "css",
    "js",
    "jsLib",
    "html"
    ));

gulp.task("start", gulp.series("dist", "server"));

